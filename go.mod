module gitea.com/crumv/backend

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.3 // indirect
	gorm.io/gorm v1.21.11 // indirect
)
